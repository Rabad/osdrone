//******************************************************************************
// File: sensorSPI.c
// Description: Library to manage the sensor MPU9250
// Author: Ruben Abad
//******************************************************************************

#include "sensorSPI.h"
#include "pcbdefs.h"
#include "stm32f30x_spi.h"



void SPI_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef  SPI_InitStructure;
  
  /* Enable the SPI peripheral */
  RCC_APB1PeriphClockCmd(SPI_CLK, ENABLE);
  
  /* Enable SCK, MOSI, MISO and NSS GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  
  /* SPI pin mappings */
  GPIO_PinAFConfig(SPI_PORT, SPI_SCK_PIN_SOURCE, SPI_AF);
  GPIO_PinAFConfig(SPI_PORT, SPI_MISO_PIN_SOURCE, SPI_AF);
  GPIO_PinAFConfig(SPI_PORT, SPI_MOSI_PIN_SOURCE, SPI_AF);
  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  
  /* SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = SPI_SCK_PIN | SPI_MISO_PIN | SPI_MOSI_PIN;
  GPIO_Init(SPI_PORT, &GPIO_InitStructure);
  
  /*SPI CS*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  
  GPIO_InitStructure.GPIO_Pin = SPI_CE_MPU9250_PIN;
  GPIO_Init(SPI_PORT, &GPIO_InitStructure);
  
  /* SPI configuration -------------------------------------------------------*/
  SPI_I2S_DeInit(SPI);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_DataSize = 8;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_Init(SPI, &SPI_InitStructure);
  
  /* Configure the Priority Group to 1 bit */                
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
  
  /* Configure the SPI interrupt priority */
  NVIC_InitStructure.NVIC_IRQChannel = SPI_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
    /* Initialize the FIFO threshold */
  SPI_RxFIFOThresholdConfig(SPI, SPI_RxFIFOThreshold_QF);
  
  /* Enable the Rx buffer not empty interrupt */
  SPI_I2S_ITConfig(SPI, SPI_I2S_IT_RXNE, ENABLE);
  /* Enable the Tx Error interrupt */
  SPI_I2S_ITConfig(SPI, SPI_I2S_IT_TXE, ENABLE);
  
  /* Enable the SPI peripheral */
  SPI_Cmd(SPI, ENABLE);
}

void SPI3_IRQHandler(void)
{
  
  /* SPI in Slave Tramitter mode--------------------------------------- */
  if (SPI_I2S_GetITStatus(SPI, SPI_I2S_IT_TXE) == SET)
  {
    SPI->DR = 
    SPI_SendData8(SPIx, TxBuffer[Tx_Idx++]);
    if (Tx_Idx == GetVar_NbrOfData())
    {
      /* Disable the Tx buffer empty interrupt */
      SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, DISABLE);
    }
  }
  
  /* SPI in Slave Receiver mode--------------------------------------- */
  if (SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_RXNE) == SET)
  {
    if (CmdReceived == 0x00)
    {
      CmdReceived = SPI_ReceiveData8(SPIx);
      CmdStatus = 0x01;
    }
    else
    {
      RxBuffer[Rx_Idx++] = SPI_ReceiveData8(SPIx);
    }
  }
}
