//******************************************************************************
//** File: pcbdefs.h
//** Description: Hardware definition for PCB
//** Author: Ruben Abad
//******************************************************************************

#ifndef PCBDEFS_INC
#define PCBDEFS_INC

//** SPI **//

#define SPI                    SPI2
#define SPI_CLK                RCC_APB1Periph_SPI2
#define SPI_IRQn               SPI2_IRQn

#define SPI_SCK_PIN            GPIO_Pin_13   
#define SPI_SCK_PIN_SOURCE     GPIO_PinSource13  
#define SPI_MISO_PIN           GPIO_Pin_14   
#define SPI_MISO_PIN_SOURCE    GPIO_PinSource14   
#define SPI_MOSI_PIN           GPIO_Pin_15   
#define SPI_MOSI_PIN_SOURCE    GPIO_PinSource15   
                               
#define SPI_PORT_CLK           RCC_AHBPeriph_GPIOC
#define SPI_PORT               GPIOC                           
#define SPI_AF                 GPIO_AF_5

#define SPI_CE_MPU9250_PORT    GPIOA
#define SPI_CE_MPU9250_PIN     GPIO_Pin_8
#define SPI_CE_MPU9250_HI()    SPI_CE_MPU9250_PORT->BSRR |= SPI_CE_MPU9250_PIN
#define SPI_CE_MPU9250_LO()    SPI_CE_MPU9250_PORT->BRR |= SPI_CE_MPU9250_PIN

#endif