//******************************************************************************
// File: sensorSPI.h
// Description: Init the SPI and the sensors that use it
// Author: Ruben Abad
//******************************************************************************
//** Includes  **//

#include "arm_math.h"

#ifndef MPU9250_INC
#define MPU9250_INC

//**  Defines  **//
//SPI

//MPU9250
#define DIR_0_IMU_MPU9250          0b1101000
#define Dir_1_IMU_MPU9250          0b1101001
#define Dir_MAG_MPU9250            0x0C


#define IMU_MPU9250_INT_PIN_CFG    0x37
#define IMU_MPU9250_SMPLRT_DIV     0x19
#define IMU_MPU9250_USER_CTRL      0x6A
#define IMU_MPU9250_PWR_MGMT_1     0x6B
#define IMU_MPU9250_MAG_CTL1       0x0A
#define IMU_MPU9250_MAG_HXL        0x03

//pins
#define IMU_9250_CE_HIGH()
#define IMU_9250_CE_LOW()

//******************************************************************************
//**  Typedefs  **//

typedef enum {
  IMU9250_DLPF_CFG_0, 
  IMU9250_DLPF_CFG_1, 
  IMU9250_DLPF_CFG_2, 
  IMU9250_DLPF_CFG_3, 
  IMU9250_DLPF_CFG_4, 
  IMU9250_DLPF_CFG_5, 
  IMU9250_DLPF_CFG_6, 
  IMU9250_DLPF_CFG_7
}
tpIMU9250_DLPF_CFG;

typedef enum {
  IMU9250_Gain_Gyro_250,
  IMU9250_Gain_Gyro_500, 
  IMU9250_Gain_Gyro_1000,
  IMU9250_Gain_Gyro_2000
}tpIMU9250_Gain_Gyro;

typedef enum {
  IMU9250_DLPF_CFG_GYRO_0, 
  IMU9250_DLPF_CFG_GYRO_1, 
  IMU9250_DLPF_CFG_GYRO_2, 
  IMU9250_DLPF_CFG_GYRO_3, 
  IMU9250_DLPF_CFG_GYRO_4, 
  IMU9250_DLPF_CFG_GYRO_5, 
  IMU9250_DLPF_CFG_GYRO_6, 
  IMU9250_DLPF_CFG_GYRO_7}
tpIMU9250_DLPF_CFG_GYRO;

typedef enum {
  IMU9250_Gain_Acel_2G, 
  IMU9250_Gain_Acel_4G,
  IMU9250_Gain_Acel_8G,
  IMU9250_Gain_Acel_16G}
tpIMU9250_Gain_Acel;

typedef enum {
  IMU9250_DLPF_CFG_ACEL_0, 
  IMU9250_DLPF_CFG_ACEL_1, 
  IMU9250_DLPF_CFG_ACEL_2, 
  IMU9250_DLPF_CFG_ACEL_3,
  IMU9250_DLPF_CFG_ACEL_4, 
  IMU9250_DLPF_CFG_ACEL_5,
  IMU9250_DLPF_CFG_ACEL_6,
  IMU9250_DLPF_CFG_ACEL_7
}tpIMU9250_DLPF_CFG_ACEL;

typedef struct{
  uint8_t addresIMU;
  uint8_t adressMAG;
  uint8_t SMPLRT_DIV;
  float32_t Sens_Gyro;
  float32_t Sens_Acel;
  float32_t Sens_Compass;
  
  tpIMU9250_DLPF_CFG DLPF_CFG_GYRO;
  tpIMU9250_Gain_Gyro Gain_Gyro;
  tpIMU9250_DLPF_CFG_GYRO DLPF_CFG_ACEL;
  tpIMU9250_Gain_Acel Gain_Acel;
}tpIMU9250;

typedef union{
  struct{
    uint8_t x_acel_l;	
    uint8_t x_acel_h;	
    uint8_t y_acel_l;
    uint8_t y_acel_h;
    uint8_t z_acel_l;
    uint8_t z_acel_h;
    
    uint8_t temp_l;
    uint8_t temp_h;
    
    uint8_t x_vel_l;
    uint8_t x_vel_h;
    uint8_t y_vel_l;
    uint8_t y_vel_h;
    uint8_t z_vel_l;
    uint8_t z_vel_h;
    
    uint8_t x_mag_l;
    uint8_t x_mag_h;
    uint8_t y_mag_l;
    uint8_t y_mag_h;
    uint8_t z_mag_l;
    uint8_t z_mag_h;
  }Reg;
  
  struct{
    int16_t x_acel;
    int16_t y_acel;
    int16_t z_acel;
    
    int16_t temp;
    
    int16_t x_vel;
    int16_t y_vel;
    int16_t z_vel;
    
    int16_t x_mag;
    int16_t y_mag;
    int16_t z_mag;
  }Valor;
}tpRead9DOF_IMU;

//******************************************************************************
//** Functions  **//

void SPI_Config(void);

void Init_IMU9250_SPI(tpIMU9250 imu9250);
void Read_IMU9250(tpRead9DOF_IMU read);

#endif